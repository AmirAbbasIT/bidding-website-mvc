﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BidMee.Entities
{
    public class AuctionPicture
    {
        public int ID { get; set; }
        public int AuctionID { get; set; }
        public int PictureID { get; set; }
        public virtual Picture Picture { get; set; }

    }
}
