﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BidMee.Entities
{
    public class Auction
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string ShortDiscription { get; set; }
        public string LongDiscription { get; set; }
        public decimal ActualPrice { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual List<AuctionPicture> Pictures { get; set; }

        //Catagory Nav Prop
        public int CatagoryID { get; set; }
        public virtual Catagory Catagory { get; set; }
    }
}
