﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BidMee.Entities
{
   public class Picture
    {
        public int ID { get; set; }
        public string URL { get; set; }
    }
}
