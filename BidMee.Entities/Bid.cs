﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BidMee.Entities
{
    public class Bid
    {
        public int ID { get; set; }
        public decimal  Bidprice { get; set; }

        public int AuctionID { get; set; }
        public virtual Auction Auction { get; set; }

        public string UserID { get; set; }
        public virtual BidMeeUser User { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
