﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BidMee.Entities
{
    public class Comment
    {
        public int ID { get; set; }
        public string Text { get; set; }
        public int Ratings { get; set; }
        public DateTime TimeStamp { get; set; }
        //To Identify Record OF a Particular Record
        public string UserID { get; set; }
        public virtual BidMeeUser User { get; set; }
        public int EntityID { get; set; }
        public int RecordID { get; set; }
    }
    //EnumType would be used for identifying for which entity coment is placed... i.e for auction id maybe=1;
    public enum EntityType
    {
        Auction=1,
        Blog=2
    }
}
