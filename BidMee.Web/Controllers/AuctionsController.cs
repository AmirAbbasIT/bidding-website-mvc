﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BidMee.Data.Services;
using BidMee.Entities;
using BidMee.Web.ViewModels;
using Microsoft.AspNet.Identity;

namespace BidMee.Web.Controllers
{
    public class AuctionsController : Controller
    {
        
        public ActionResult Index()
        {
           
            return View();
          
        }
        public ActionResult AuctionDetail(int ID)
        {
            var model = new detailAuctionVM();
            model.Auction = AuctionServices.GetAuctionServices().GetAuction(ID);
            model.EntityType = (int)EntityType.Auction;
            model.Comments = CommentServices.GetCommentServices().GetComments((int)EntityType.Auction,ID);
            model.HighestBidder = BidServices.GetBidServices().getHighestBidder(ID);
            model.PictureUrls = AuctionServices.GetAuctionServices().GetAuctionPictures(ID);
            return View(model);
        }

        #region AuctionsIndex
        public ActionResult AuctionTable(string search,int? pageNo,int? pageSize=5)
        {
            pageNo = (pageNo == null) ? 1 :pageNo;
            var model = new AuctionsVM();
            model.auctions = AuctionServices.GetAuctionServices().GetAllAuctions(search,(int)pageNo,(int)pageSize).ToList();
            model.searchTerm = search;
            model.pager = new BaseListingVM.Pager(AuctionServices.GetAuctionServices().AuctionCount(search), pageNo, (int)pageSize);
            return PartialView(model);
        }
        #endregion

        #region Add
        [HttpGet]
        public ActionResult AddAuction()
        {
            createAuctionVM model = new createAuctionVM()
            {
                Catagories = CatagoryServices.GetCatagoryServices().GetCatagories()
            };
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult AddAuction(Auction auction,string PicIDs)
        {
            AuctionServices.GetAuctionServices().AddAuction(auction,PicIDs);
            return RedirectToAction("AuctionTable");
        }
        #endregion

        #region PostComment
        [HttpPost]
        public JsonResult PostComment(Comment comment)
        {
            comment.TimeStamp = DateTime.Now;
            comment.UserID = User.Identity.GetUserId();
            comment.Ratings = comment.Ratings;
            CommentServices.GetCommentServices().AddComment(comment);
            JsonResult result=new JsonResult();
            result.Data =new { Success = true };
            return result ;
        }
        #endregion

        #region Edit
        [HttpGet]
        public ActionResult EditAuction(int ID)
        {
            var model = AuctionServices.GetAuctionServices().GetAuction(ID);
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditAuction(Auction auction)
        {
            AuctionServices.GetAuctionServices().EditAuction(auction);
            return RedirectToAction("AuctionTable");
        }
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult DeleteAuction(int ID)
        {
            var result=AuctionServices.GetAuctionServices().DeleteAuction(ID);
            return RedirectToAction("AuctionTable");
        }
        #endregion

        #region Bid
        [HttpPost]
        public ActionResult Bid(int auctionID,int bidPrice)
        {
            bidPrice += 10;
            var bid = new Bid
            {
                UserID = User.Identity.GetUserId(),
                AuctionID = auctionID,
                Bidprice = bidPrice,
                TimeStamp=DateTime.Now

            };
            BidServices.GetBidServices().AddBid(bid);
            return RedirectToAction("Detail","Auctions");
        }
        #endregion
    }
}