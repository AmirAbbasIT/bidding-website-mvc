﻿using BidMee.Data.Services;
using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BidMee.Web.Controllers
{
    public class SharedController : Controller
    {
        [HttpPost]
        public JsonResult AddPictures()
        {

            var pics = Request.Files;
            JsonResult result = new JsonResult();
            List<object> pictureJson = new List<object>();
            if(pics!=null)
            {
                for (int i = 0; i < pics.Count; i++)
                {
                    var picture = pics[i];
                    var fileName = Guid.NewGuid() + Path.GetExtension(picture.FileName);
                    var path = Server.MapPath("~/Content/Images/") + fileName;
                    picture.SaveAs(path);
                    var dbPicture = new Picture();
                    dbPicture.URL = fileName;
                    dbPicture.ID = PictureServices.GetPictureServices().AddPicture(fileName);
                    ////StartFrom Here
                    pictureJson.Add(dbPicture);
                }
                result.Data = pictureJson;
            }
            return result;
        }

        [HttpPost]
        public JsonResult DeletePicture(int ID)
        {
            //string fullPath = Request.MapPath("~/Images/Cakes/" + photoName);
            //if (System.IO.File.Exists(fullPath))
            //{
            //    System.IO.File.Delete(fullPath);
            //}
            return new JsonResult() { };
        }
    }
}