﻿using BidMee.Data.Services;
using BidMee.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BidMee.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? pageNo,int? pageSize=12)
        {
            pageNo = (pageNo == null) ? 1 : pageNo;
            HomeVM model = new HomeVM();
            model.Auctions = AuctionServices.GetAuctionServices().GetAllAuctions(null,(int)pageNo,(int)pageSize).ToList();
            model.pager = new BaseListingVM.Pager(AuctionServices.GetAuctionServices().AuctionCount(null), pageNo, (int)pageSize);
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}