﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BidMee.Web.Controllers
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult Auctions()
        {
            return RedirectToAction("Index","Auctions");
        }
        public ActionResult Catagories()
        {
            return RedirectToAction("Index","Catagory");
        }
    }
}