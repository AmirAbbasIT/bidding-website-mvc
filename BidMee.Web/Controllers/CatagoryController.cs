﻿using BidMee.Data.Services;
using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BidMee.Web.Controllers
{
    public class CatagoryController : Controller
    {
        public ActionResult Index()
        {
            return View();
        } 

        #region CatagoriesTable
        public ActionResult CatagoryTable()
        {
            var model = CatagoryServices.GetCatagoryServices().GetCatagories();
            return PartialView(model);
        }
        #endregion

        #region Add
        [HttpGet]
        public ActionResult AddCatagory()
        {
            return PartialView();
        }
        [HttpPost]
        public ActionResult AddCatagory(Catagory catagory)
        {
            CatagoryServices.GetCatagoryServices().AddCatagory(catagory);
            return RedirectToAction("CatagoryTable");
        }
        #endregion

        #region Edit
        [HttpGet]
        public ActionResult EditCatagory(int ID)
        {
            var model = CatagoryServices.GetCatagoryServices().GetCatagory(ID);
            return PartialView(model);
        }
        [HttpPost]
        public ActionResult EditCatagory(Catagory catagory)
        {
            CatagoryServices.GetCatagoryServices().EditCatagory(catagory);
            return RedirectToAction("CatagoryTable");
        }
        #endregion

        #region Delete
        [HttpPost]
        public ActionResult DeleteCatagory(int ID)
        {
            var result = CatagoryServices.GetCatagoryServices().DeleteCatagory(ID);
            return Redirect(Url.Action("CatagoryTable","Catagory"));
        }
        #endregion

    }
}