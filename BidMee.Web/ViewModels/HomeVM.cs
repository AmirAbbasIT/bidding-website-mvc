﻿using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static BidMee.Web.ViewModels.BaseListingVM;

namespace BidMee.Web.ViewModels
{
    public class HomeVM
    {
        public List<Auction> Auctions { get; set; }
        public Pager pager { get; set; }
    }
}