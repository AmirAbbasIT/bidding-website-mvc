﻿using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static BidMee.Web.ViewModels.BaseListingVM;

namespace BidMee.Web.ViewModels
{
    public class AuctionsVM
    {
        public List<Auction> auctions { get; set; }
        public string searchTerm { get; set; }
        public Pager pager { get; set; }
    }
    public class createAuctionVM
    {
        public List<Catagory> Catagories { get; set; }
    }
    public class detailAuctionVM
    {
        public Auction Auction { get; set; }
        public int EntityType { get; set; }
        public List<Comment> Comments { get; set; }
        public Bid HighestBidder { get; set; }
        public List<Picture> PictureUrls { get; set; }
    }
}