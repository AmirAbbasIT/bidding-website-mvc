namespace BidMee.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingCommentEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    Text = c.String(),
                    Ratings = c.Int(nullable: false),
                    UserID = c.String(),
                    EntityID = c.Int(nullable: false),
                    RecordID = c.Int(nullable: false),
                    Auction_ID = c.Int(),
                })
                .PrimaryKey(t => t.ID);
                
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Comments");
        }
    }
}
