namespace BidMee.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingUserRelationtoComments : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "UserID", c => c.String(maxLength: 128));
            CreateIndex("dbo.Comments", "UserID");
            AddForeignKey("dbo.Comments", "UserID", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Comments", new[] { "UserID" });
            AlterColumn("dbo.Comments", "UserID", c => c.String());
        }
    }
}
