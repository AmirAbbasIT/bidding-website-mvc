namespace BidMee.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddingCatagoryEntity : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Catagories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Auctions", "CatagoryID", c => c.Int(nullable: true));
            CreateIndex("dbo.Auctions", "CatagoryID");
            AddForeignKey("dbo.Auctions", "CatagoryID", "dbo.Catagories", "ID", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Auctions", "CatagoryID", "dbo.Catagories");
            DropIndex("dbo.Auctions", new[] { "CatagoryID" });
            DropColumn("dbo.Auctions", "CatagoryID");
            DropTable("dbo.Catagories");
        }
    }
}
