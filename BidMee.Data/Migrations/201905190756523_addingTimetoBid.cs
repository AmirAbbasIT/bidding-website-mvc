namespace BidMee.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingTimetoBid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bids", "TimeStamp", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bids", "TimeStamp");
        }
    }
}
