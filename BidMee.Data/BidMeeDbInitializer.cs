﻿using BidMee.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BidMee.Data
{
    public class BidMeeDbInitializer : CreateDatabaseIfNotExists<BidMeeContext>
    {
        protected override void Seed(BidMeeContext context)
        {
            GenerateRoles(context);
            GenerateUser(context);
        }

        #region Generate InitialUsers & Roles for Admin
        public void GenerateUser(BidMeeContext context)
        {
            var userStore = new UserStore<BidMeeUser>(context);
            var userManager = new UserManager<BidMeeUser>(userStore);
            var User = new BidMeeUser()
            {
                Email="admin@mail.com",
                UserName="Admin"
            };
            var password = "admin123";
            if(userManager.FindByEmail(User.Email)==null)
            {
                var result = userManager.Create(User,password);
                if(result.Succeeded)
                {
                    userManager.AddToRole(User.Id, "Administrator");
                    userManager.AddToRole(User.Id, "User");
                }
            }
        }
        public void GenerateRoles(BidMeeContext context)
        {
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);
            //Creating Default Roles.....
            var listofRoles = new List<IdentityRole>();
            listofRoles.Add(new IdentityRole() { Name = "Administrator" });
            listofRoles.Add(new IdentityRole() { Name = "User" });
            foreach(IdentityRole role in listofRoles)
            {
                var result=roleManager.Create(role);
                //////////
            }
        }
        #endregion
    }
}
