﻿using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BidMee.Data.Services
{
    public class CommentServices
    {
        private static CommentServices obj;

        public static CommentServices GetCommentServices()
        {
            if (obj == null)
            {
                obj = new CommentServices();
            }
            return obj;
        }
        public List<Comment> GetComments(int EnityID,int RecordID)
        {

            using (var db = new BidMeeContext())
            {
                return db.Comments.Where(c => c.EntityID == EnityID && c.RecordID == RecordID).Include(m => m.User).ToList();
               
            }
        }
        public void AddComment(Comment comment)
        {
            if(comment!=null)
            {
                using (var db = new BidMeeContext())
                {
                    db.Comments.Add(comment);
                    db.SaveChanges();
                }
            }
        }
    }
}
