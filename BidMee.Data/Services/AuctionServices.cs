﻿using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BidMee.Data.Services
{
    public class AuctionServices
    {
        private static AuctionServices obj;
        
        public static AuctionServices GetAuctionServices()
        {
            if(obj==null)
            {
                obj = new AuctionServices();
            }
            return obj;
        }
        #region AuctionCount
        public int AuctionCount(string search)
        {
            int count;
            using (var db = new BidMeeContext())
            {
                if(string.IsNullOrEmpty(search))
                {
                    count = db.Auctions.Count();
                }
                else
                {
                    count = db.Auctions.Where(a => a.Title.ToLower().Contains(search.ToLower())).Count();
                }
                
            }
                return count;
        }
        #endregion

        #region GetAllAuctions
        public IList<Auction> GetAllAuctions(string search,int pageNo,int pageSize)
        {
            var auctions=new List<Auction>();
            using (var db= new BidMeeContext())
            {
                auctions = db.Auctions.ToList();
            }
            if(!string.IsNullOrEmpty(search))
            {
                auctions= auctions.Where(a => a.Title.ToLower().Contains(search.ToLower()))
                         .Skip((pageNo - 1) * pageSize)
                         .Take(pageSize)
                         .ToList();
            }
            else
            {
                auctions=auctions
                    .Skip((pageNo - 1) * pageSize)
                    .Take(pageSize)
                    .ToList();
            }
            return auctions;
        }
        #endregion

        #region GetSingleAuction
        public Auction GetAuction(int ID)
        {
            var auction = new Auction();
            using (var db = new BidMeeContext())
            {
                auction = db.Auctions.Where(a => a.ID == ID).FirstOrDefault();
            }
            return auction;
        }
        #endregion

        #region AddAuction
        public void AddAuction(Auction auction,string pics)
        {
            var picIDs = pics.Split(',').Select(ID=>int.Parse(ID)).ToList();
            auction.Pictures = new List<AuctionPicture>();
            auction.Pictures.AddRange(picIDs.Select(ID=>new AuctionPicture() { PictureID=ID}));
            if(auction!=null)
            {
                using (var db = new BidMeeContext())
                {
                    db.Auctions.Add(auction);
                    db.SaveChanges();
                }
            }
        }
        #endregion

        #region DeleteAuction
        public bool DeleteAuction(int ID)
        {
            using (var db = new BidMeeContext())
            {
                var auction = db.Auctions.Where(a => a.ID == ID).SingleOrDefault();
                if(auction!=null)
                {
                    db.Auctions.Remove(auction);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditAuction
        public void EditAuction(Auction auction)
        {
            using (var db = new BidMeeContext())
            {
                var editedauction = db.Auctions.Where(a => a.ID == auction.ID).SingleOrDefault();
                if(editedauction!=null)
                {
                    editedauction.Title = auction.Title;
                    editedauction.ShortDiscription = auction.ShortDiscription;
                    editedauction.LongDiscription = auction.LongDiscription;
                    editedauction.StartDate = auction.StartDate;
                    editedauction.EndDate = auction.EndDate;
                    editedauction.ActualPrice = auction.ActualPrice;
                    db.Auctions.Attach(editedauction);
                    db.Entry(editedauction).State= EntityState.Modified;
                    db.SaveChanges();
                }
            }
                
        }
        #endregion

        #region GetAuctionPictures
        public List<Picture> GetAuctionPictures(int ID)
        {
            using (var db = new BidMeeContext())
            {
                var AuctionPictures = db.AuctionPictures.Where(a => a.AuctionID==ID).Select(p=>p.PictureID).ToList();
                var Pictures = PictureServices.GetPictureServices().GetPictures(AuctionPictures.ToArray());
                return Pictures;
            }
        }
        #endregion
    }
}
