﻿using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BidMee.Data.Services
{
    public class CatagoryServices
    {
        private static CatagoryServices obj;

        public static CatagoryServices GetCatagoryServices()
        {
            if (obj == null)
            {
                obj = new CatagoryServices();
            }
            return obj;
        }

        #region GetAll Catagories
        public List<Catagory> GetCatagories()
        {
            using (var db = new BidMeeContext())
            {
                return db.Catagories.ToList();
            }
        }
        #endregion

        #region GetSingleCatagory
        public Catagory GetCatagory(int ID)
        {
            var catagory = new Catagory();
            using (var db = new BidMeeContext())
            {
                catagory = db.Catagories.Where(a => a.ID == ID).FirstOrDefault();
            }
            return catagory;
        }
        #endregion

        #region AddCatagory
        public void AddCatagory(Catagory catagory)
        {

            using (var db = new BidMeeContext())
            {
                db.Catagories.Add(catagory);
                db.SaveChanges();
            }

        }
        #endregion

        #region DeleteCatagory
        public bool DeleteCatagory(int ID)
        {
            using (var db = new BidMeeContext())
            {
                var catagory = db.Catagories.Where(a => a.ID == ID).SingleOrDefault();
                if (catagory != null)
                {
                    db.Catagories.Remove(catagory);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }

            }
        }
        #endregion

        #region EditCatagory
        public void EditCatagory(Catagory catagory)
        {
            using (var db = new BidMeeContext())
            {
                var editedCatagory = db.Catagories.Where(a => a.ID == catagory.ID).SingleOrDefault();
                if (editedCatagory != null)
                {
                    editedCatagory.Title = catagory.Title;
                    db.Catagories.Attach(editedCatagory);
                    db.Entry(editedCatagory).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

        }
        #endregion


    }
}
