﻿using BidMee.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BidMee.Data.Services
{
    public class BidServices
    {
        private static BidServices obj;

        public static BidServices GetBidServices()
        {
            if (obj == null)
            {
                obj = new BidServices();
            }
            return obj;
        }
        public void AddBid(Bid bid)
        {
            using(var db=new BidMeeContext())
            {
                db.Bids.Add(bid);
                db.SaveChanges();
            }
        }
        public Bid getHighestBidder(int auctionID)
        {
            using (var db = new BidMeeContext())
            {
                return db.Bids.Where(b => b.AuctionID == auctionID).OrderByDescending(b=>b.Bidprice).Include(b=>b.User).FirstOrDefault();
            }
        }
    }
}
