﻿using BidMee.Entities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BidMee.Data.Services
{
    public class BidMeeSignInManager : SignInManager<BidMeeUser, string>
    {
        public BidMeeSignInManager(BidMeeUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(BidMeeUser user)
        {
            return user.GenerateUserIdentityAsync((BidMeeUserManager)UserManager);
        }

        public static BidMeeSignInManager Create(IdentityFactoryOptions<BidMeeSignInManager> options, IOwinContext context)
        {
            return new BidMeeSignInManager(context.GetUserManager<BidMeeUserManager>(), context.Authentication);
        }
    }
}
