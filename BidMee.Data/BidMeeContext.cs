﻿using BidMee.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BidMee.Data
{
    public class BidMeeContext : IdentityDbContext<BidMeeUser>
    {
        public DbSet<Auction> Auctions { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<AuctionPicture> AuctionPictures { get; set; }
        public DbSet<Catagory> Catagories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Bid> Bids { get; set; }
        public BidMeeContext():base("name=BidMeeDatabase")
        {
            Database.SetInitializer<BidMeeContext>(new BidMeeDbInitializer());
        }
        public static BidMeeContext Create()
        {
            return new BidMeeContext();
        }
    }
}
